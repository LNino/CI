package lnino.generalisation;


import org.geotools.geometry.jts.JTSFactoryFinder;
import org.junit.Test;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

import junit.framework.TestCase;
import lnino.exception.EpsilonException;
import lnino.generalisation.GeneralisationLineStrings;

public class GeneralisationLineStringsTest extends TestCase {

	@Test
	public void testPolylignesNoNull() throws EpsilonException {
		
		Coordinate c = new Coordinate(10,10);
		Coordinate c2 = new Coordinate(20,20);
		Coordinate[] points = {c,c2};
		GeneralisationLineStrings pline = new GeneralisationLineStrings(10);
		
		Coordinate[] result = pline.fonctionDouglasPeucker(points, 0, points.length - 1);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		LineString line = geometryFactory.createLineString(result);
		
		assertNotNull(line);
	}
	
	@Test
	public void testPolylignesPetiteValeurEpsilon() throws EpsilonException {
		
		Coordinate c = new Coordinate(10,10);
		Coordinate c2 = new Coordinate(20,20);
		Coordinate[] points = {c,c2};
		GeneralisationLineStrings pline = new GeneralisationLineStrings(0.00001);
		
		LineString ls = pline.createLineString(points);
		
		Coordinate[] result = pline.fonctionDouglasPeucker(points, 0, points.length - 1);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		LineString line = geometryFactory.createLineString(result);
		
		assertEquals(ls, line);
	}
	
	@Test
	public void testPolylignesGrandeValeurEpsilon() throws EpsilonException {
		
		Coordinate c = new Coordinate(10,10);
		Coordinate c2 = new Coordinate(20,20);
		Coordinate[] points = {c,c2};
		GeneralisationLineStrings pline = new GeneralisationLineStrings(1000000);
		
		LineString ls = pline.createLineString(points);
		
		Coordinate[] result = pline.fonctionDouglasPeucker(points, 0, points.length - 1);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		LineString line = geometryFactory.createLineString(result);
		
		assertEquals(ls, line);
	}
	
	@Test
	public void testTaillePolylignes() throws EpsilonException {
		Coordinate c = new Coordinate(1,1);
		Coordinate c2 = new Coordinate(2,2);
		Coordinate c3 = new Coordinate(3,3);
		Coordinate c4 = new Coordinate(8,1);
		
		Coordinate[] points = {c,c2,c3,c4};
		
		GeneralisationLineStrings pline = new GeneralisationLineStrings(5);
		
		Coordinate[] result = pline.fonctionDouglasPeucker(points, 0, points.length - 1);
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		LineString newline = geometryFactory.createLineString(result);
		
		LineString line = geometryFactory.createLineString(points);
		
		assertTrue(line.getLength() >= newline.getLength());
	}
	
	@Test
	public void testDistance() throws EpsilonException {
		Coordinate c = new Coordinate(1,1);
		Coordinate c2 = new Coordinate(2,2);
		Coordinate c3 = new Coordinate(3,3);
		Coordinate c4 = new Coordinate(8,1);
		Coordinate c5 = new Coordinate(10,10);
		
		Coordinate[] points = {c,c2,c3,c4};
		
		GeneralisationLineStrings pline = new GeneralisationLineStrings(5);
		double myDistance = pline.distance(points, 0, points.length - 1, c5);
		
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		LineString line = geometryFactory.createLineString(points);
		Point point = geometryFactory.createPoint(c5);
		double jtsDistance = line.distance(point);
		
		assertEquals(myDistance, jtsDistance, 0.5);
	}
	
	@Test
	public void testCreatePoint() throws EpsilonException {
		GeneralisationLineStrings nline = new GeneralisationLineStrings(10);
		
		Coordinate c = new Coordinate(1,1);
		Coordinate c2 = new Coordinate(5,9);
		Coordinate c3 = new Coordinate(3,3);
		Coordinate c4 = new Coordinate(8,1);
		
		Coordinate[] points = {c,c2,c3,c4};
		
		assertEquals(nline.createLineString(points).getGeometryType(), "LineString");
		
	}
	
	

}
