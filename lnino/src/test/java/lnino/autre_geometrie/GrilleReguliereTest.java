package lnino.autre_geometrie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;

import lnino.exception.PasException;

public class GrilleReguliereTest {

	@Test
	public void testCreateEnvelopes() throws PasException, IOException {
		GrilleReguliere grille = new GrilleReguliere(10);
		
		assertNotNull(grille.createEnvelopes(0, 1, 0, 1));
	}
	
	@Test
	public void testReturnEnvelopes() throws PasException, IOException {
		GrilleReguliere grille = new GrilleReguliere(10);
		
		assertEquals(grille.createEnvelopes(0, 100, 0, 100).getClass().getSimpleName(), "Envelope[][]");
	}

	@Test
	public void testTailleCreateEnvelopes() throws PasException, IOException {
		GrilleReguliere grille = new GrilleReguliere(10);
		
		assertEquals(grille.createEnvelopes(0, 100, 0, 100).length, grille.getNbEnv() + 1);
	}

}
