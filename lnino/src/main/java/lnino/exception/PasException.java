package lnino.exception;

public class PasException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PasException(){
		System.out.println("Le pas doit être supérieur à 1");
	}

}
