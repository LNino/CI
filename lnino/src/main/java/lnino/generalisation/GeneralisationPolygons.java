package lnino.generalisation;

import java.io.File;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import lnino.exception.PasException;
import lnino.shapefile.GestionShapeFile;

/**
 * Classe permettant la simplification d'un polygone
 * 
 * @author lnino
 * @version 1.0
 */
public class GeneralisationPolygons extends AbstractGeneralisation implements GestionShapeFile{
	
	SimpleFeatureSource featureSource;
	
	/**
	 * Constructeur de la classe GeneralisationPolygons
	 * 
	 */
	public GeneralisationPolygons() {
		
	}
	
	/**
	 * Cette métohde permet de récurpérer les coordonnées de la boîte de délimitation du polygon
	 * @param p {@link Polygon}
	 * 		à simplifier
	 * @return {@link Coordinate[]}
	 */
	public Coordinate[] genePolygon(Polygon p){
		double x = p.getEnvelopeInternal().getMinX();
		double xp = p.getEnvelopeInternal().getMaxX();
		double y = p.getEnvelopeInternal().getMinY();
		double yp = p.getEnvelopeInternal().getMaxY();
		
		Coordinate c1 = new Coordinate(x, y);
		Coordinate c2 = new Coordinate(x, yp);
		Coordinate c3 = new Coordinate(xp, yp);
		Coordinate c4 = new Coordinate(xp, y);
		
		Coordinate[] coords = {c1, c2, c3, c4, c1};
		return coords;
	}
	
	/**
	 * Cette méthode permet de construire un polygone à partir d'une liste de coordonnées
	 * @param coords {@link Coordinate[]}
	 * 		
	 * @return {@link Polygon}
	 */
	public Polygon createPolygone(Coordinate[] coords){
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		return geometryFactory.createPolygon(coords);
	}

	@Override
	public void createNewShapeFile() throws SchemaException, IOException {
		
		File newShape = new File("polygon.shp");
		SimpleFeatureType type = DataUtilities.createType("polygon","the_geom:Polygon:srid=2154");
		
		ShapefileDataStoreFactory factory = new ShapefileDataStoreFactory();
		
		Map<String, Serializable> newMap = new HashMap<String, Serializable>();
		
		newMap.put("url", newShape.toURI().toURL());
		
		ShapefileDataStore newStore = (ShapefileDataStore) factory.createNewDataStore(newMap);
		
		newStore.createSchema(type);
		
		List<SimpleFeature> features = new ArrayList<SimpleFeature>();
			
		SimpleFeatureBuilder featureBuild = new SimpleFeatureBuilder(type);
		
		Transaction transaction = new DefaultTransaction("create");
		
		SimpleFeatureCollection featCollection = this.featureSource.getFeatures();
		SimpleFeatureIterator iterator = featCollection.features();
		
		GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		
		while(iterator.hasNext()) {
			
			SimpleFeature feature = iterator.next();
			Geometry geom = (Geometry) feature.getAttribute(0);
			MultiPolygon poly = (MultiPolygon) geom;
			
			Coordinate[] cs =poly.getCoordinates();
	        Polygon p = geometryFactory.createPolygon(cs);
		 		 
		 featureBuild.add(this.createPolygone(this.genePolygon(p)));
		 SimpleFeature newFeature = featureBuild.buildFeature(null);
		 features.add(newFeature);
		}
		
		 String typeName = newStore.getTypeNames()[0];
		 SimpleFeatureSource featureSource = newStore.getFeatureSource(typeName);
		 featureSource.getSchema();
		 if (featureSource instanceof SimpleFeatureStore) {
             SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
    		 SimpleFeatureCollection newCollect = new ListFeatureCollection(type, features);
             featureStore.setTransaction(transaction);
             try {
                 featureStore.addFeatures(newCollect);
                 transaction.commit();
             } catch (Exception problem) {
                 problem.printStackTrace();
                 transaction.rollback();
             } 
         }
		
		transaction.close();
		newStore.dispose();	
		
	}

	@Override
	public void openShapeFile() throws IOException {
		File file = JFileDataStoreChooser.showOpenFile("shp", null);
		if(file == null){
			return;
		}
		
		FileDataStore store = FileDataStoreFinder.getDataStore(file);
		this.featureSource = store.getFeatureSource();
        MapContent map = new MapContent();
		
		Style style = SLD.createSimpleStyle(featureSource.getSchema());
		Layer layer = new FeatureLayer(featureSource, style);
		map.addLayer(layer);
		
		JMapFrame.showMap(map);
	}

	@Override
	public Coordinate[] fonctionDouglasPeucker(Coordinate[] points, int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LineString createLineString(Coordinate[] pts) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Coordinate> genPoint(List<Coordinate> pts, double minX, double maxX, double minY, double maxY)
			throws PasException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Point createPoint(Coordinate coords) {
		// TODO Auto-generated method stub
		return null;
	}

}
