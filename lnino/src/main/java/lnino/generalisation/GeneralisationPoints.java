package lnino.generalisation;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

import lnino.autre_geometrie.GrilleReguliere;
import lnino.exception.PasException;
import lnino.shapefile.GestionShapeFile;

/**
 * Classe permettant la simplification d'un nuage de points
 * 
 * @author lnino
 * @version 1.0
 */
public class GeneralisationPoints extends AbstractGeneralisation implements GestionShapeFile{
	
	SimpleFeatureSource featureSource;
	
	int pas;
	
	/**
	 * Constructeur de la classe GeneralisationPoints
	 * 
	 * @param pas {@link Integer}
	 *  	 correspond au pas permettant la création de la grille
	 * @throws PasException 
	 * 
	 */
	public GeneralisationPoints(int pas) throws PasException {
		if(pas <= 1){
			throw new PasException();
		}
		this.pas = pas;
	}
	
	/**
	 * Retourne la valeur pas
	 * 		
	 * @return {@link Integer} pas
	 * 
	 */
	public int getPas() {
		return pas;
	}
	
	/**
	 * Attribue un pas à une GénéralisationPoint
	 * 
	 * @param pas {@link Integer}
	 */
	public void setPas(int pas) {
		this.pas = pas;
	}
	
	/**
	 * Cette méthode calcule la moyenne des x d'une liste de coordonnées
	 * 
	 * @param coords {@link Coordinate[]}
	 * 		
	 * @return {@link Double} moyenne des x
	 */
	public double calculMoyenneX(List<Coordinate> coords){
		
		double sumX = 0;
		for(Coordinate c : coords){
			sumX += c.x;
		}
		
		return sumX/coords.size();
	}
	
	/**
	 * Cette méthode calcule la moyenne des y d'une liste de coordonnées
	 * 
	 * @param coords {@link Coordinate[]}
	 * 		
	 * @return {@link Double} moyenne des y
	 */
	public double calculMoyenneY(List<Coordinate> coords){
		
		double sumY = 0;
		for(Coordinate c : coords){
			sumY += c.y;
		}
		
		return sumY/coords.size();
	}
	
	/**
	 * Cette méthode permet de déterminer les points moyens pour chaque enveloppe de la grille
	 * @param pts {@link Coordinate[]}
	 * 		liste des coordonnées appartenant au nuage de point à simplifier
	 * @param minX {@link Double}
	 * 		x minimal de l'emprise du shapefile
	 * @param maxX {@link Double}
	 * 		x maximal de l'emprise du shapefile
	 * @param minY {@link Double}
	 * 		y minimal de l'emprise du shapefile
	 * @param maxY {@link Double}
	 * 		y maximal de l'emprise du shapefile
	 * @return {@link Coordinate[]}
	 * 		la liste de coordonnées contenant les points moyens
	 * @throws PasException 
	 */
	public List<Coordinate> genPoint(List<Coordinate> pts, double minX, double maxX, double minY, double maxY) throws PasException {
		
		GrilleReguliere grille = new GrilleReguliere(this.pas);
		
		Envelope[][] envTab = grille.createEnvelopes(minX, maxX, minY, maxY);
		
		List<Coordinate> ptsMoy = new ArrayList<>();
		
		
		for(int i = 0; i < envTab.length; i++){
			for(int j = 0; j < envTab[1].length; j++){
				
				List<Coordinate> newPoints = new ArrayList<>();
				for(int k = 0; k < pts.size(); k++){
					if(envTab[i][j].contains(pts.get(k))){
						newPoints.add(pts.get(k));
					}
				}
				double moyX = 0;
				double moyY = 0;
				if(!newPoints.isEmpty()){
					moyX = this.calculMoyenneX(newPoints);
					moyY = this.calculMoyenneY(newPoints);
					ptsMoy.add(new Coordinate(moyX, moyY));	
				}
					
			}
		}
		
		return ptsMoy;
		
	}
	
	/**
	 * Cette méthode permet de construire un point à partir d'une liste de coordonnées
	 * 
	 * @param coords {@link Coordinate[]}
	 * 		
	 * @return {@link Point} 
	 */
	public Point createPoint(Coordinate c){
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
		return geometryFactory.createPoint(c);
	}
	
	@Override
	public void createNewShapeFile() throws SchemaException, IOException, PasException {
		
		File newShape = new File("point.shp");
		SimpleFeatureType type = DataUtilities.createType("point","the_geom:Point:srid=2154");
		
		ShapefileDataStoreFactory factory = new ShapefileDataStoreFactory();
		
		Map<String, Serializable> newMap = new HashMap<String, Serializable>();
		
		newMap.put("url", newShape.toURI().toURL());
		
		ShapefileDataStore newStore = (ShapefileDataStore) factory.createNewDataStore(newMap);
		
		newStore.createSchema(type);
		
		List<SimpleFeature> features = new ArrayList<SimpleFeature>();
			
		SimpleFeatureBuilder featureBuild = new SimpleFeatureBuilder(type);
		
		Transaction transaction = new DefaultTransaction("create");
		
		SimpleFeatureCollection featCollection = this.featureSource.getFeatures();
		SimpleFeatureIterator iterator = featCollection.features();
		
		List<Coordinate> points = new ArrayList<>();
		
		while(iterator.hasNext()) {
			
			SimpleFeature feature = iterator.next();
			Geometry geom = (Geometry) feature.getAttribute(0);
			Point p = (Point) geom;
			points.add(p.getCoordinate());
		
		}
		
		for(Coordinate c : this.genPoint(points, this.featureSource.getBounds().getMinX(),this.featureSource.getBounds().getMaxX(),this.featureSource.getBounds().getMinY(),this.featureSource.getBounds().getMinY())){
			featureBuild.add(this.createPoint(c));
			SimpleFeature newFeature = featureBuild.buildFeature(null);
			features.add(newFeature);
		}
				
		String typeName = newStore.getTypeNames()[0];
		SimpleFeatureSource featureSource = newStore.getFeatureSource(typeName);
		featureSource.getSchema();
		if (featureSource instanceof SimpleFeatureStore) {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
    		SimpleFeatureCollection newCollect = new ListFeatureCollection(type, features);
            featureStore.setTransaction(transaction);
            try {
                featureStore.addFeatures(newCollect);
                transaction.commit();
            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();
            } 
        }
		
		transaction.close();
		newStore.dispose();
	}

	@Override
	public void openShapeFile() throws IOException {
		File file = JFileDataStoreChooser.showOpenFile("shp", null);
		if(file == null){
			return;
		}
		
		FileDataStore store = FileDataStoreFinder.getDataStore(file);
		this.featureSource = store.getFeatureSource();
        MapContent map = new MapContent();
		
		Style style = SLD.createSimpleStyle(featureSource.getSchema());
		Layer layer = new FeatureLayer(featureSource, style);
		map.addLayer(layer);
		
		JMapFrame.showMap(map);
	}

	@Override
	public Coordinate[] fonctionDouglasPeucker(Coordinate[] points, int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LineString createLineString(Coordinate[] pts) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Coordinate[] genePolygon(Polygon p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Polygon createPolygone(Coordinate[] coords) {
		// TODO Auto-generated method stub
		return null;
	}

}
